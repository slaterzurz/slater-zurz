Ohio law firm with over 40 years of experience in a variety of legal practice areas including personal injury, car accidents, nursing home abuse, dog bites, wrongful death, medical malpractice, legal malpractice, divorce, dissolution and much more.

Address: 1 Cascade Plaza, Akron, OH 44308, USA

Phone: 888-534-4850